﻿module FConvertString.Test

open Expecto


let testEqual (actual, expected) = 
    Expect.equal actual expected "Equality test failed"


let inline testFloatHighAccuracy (actual: 'a option, expected: 'a option) =
    match expected with
    | None ->
        Expect.isNone actual "Expected None"
    | Some x ->
        Expect.isSome actual "Expected Some but got None"
        Expect.floatClose Accuracy.high (float actual.Value) (float x) (sprintf "%A = %A failed" actual expected)


[<Tests>]
let tests =
    testList "all" [
        testCase "ToByte" <| fun _ ->
            [ (FConvertString.ToByte "45", Some 45uy)
              (FConvertString.ToByte "0", Some 0uy)
              (FConvertString.ToByte "-0", Some 0uy)
              (FConvertString.ToByte "-9", None) 
              (FConvertString.ToByte "", None)
              (FConvertString.ToByte "0xA", Some 10uy)
              (FConvertString.ToByte "459800", None)
              (FConvertString.ToByte "0xFFFFF", None)
              (FConvertString.ToByte "0xG", None)
              (FConvertString.ToByte "0x-A", None)
              (FConvertString.ToByte "09", None)
              (FConvertString.ToByte "045", Some 37uy)
              (FConvertString.ToByte "0x", None) ]
            |> List.iter testEqual

        testCase "ToSByte" <| fun _ ->
            [ (FConvertString.ToSByte "45", Some 45y)
              (FConvertString.ToSByte "0", Some 0y)
              (FConvertString.ToSByte "-0", Some 0y)
              (FConvertString.ToSByte "-9", Some -9y) 
              (FConvertString.ToSByte "", None)
              (FConvertString.ToSByte "0xA", Some 10y)
              (FConvertString.ToSByte "459800", None)
              (FConvertString.ToSByte "0xFFFFF", None)
              (FConvertString.ToSByte "0xG", None)
              (FConvertString.ToSByte "0x-A", None)
              (FConvertString.ToSByte "09", None)
              (FConvertString.ToSByte "045", Some 37y)
              (FConvertString.ToSByte "0x", None) ]
            |> List.iter testEqual

        testCase "ToUInt16" <| fun _ ->
            [ (FConvertString.ToUInt16 "45", Some 45us)
              (FConvertString.ToUInt16 "0", Some 0us)
              (FConvertString.ToUInt16 "-0", Some 0us)
              (FConvertString.ToUInt16 "-9", None)
              (FConvertString.ToUInt16 "", None)
              (FConvertString.ToUInt16 "0xA", Some 10us)
              (FConvertString.ToUInt16 "0xFFFF", Some 65535us)
              (FConvertString.ToUInt16 "0xG", None)
              (FConvertString.ToUInt16 "0x-A", None)
              (FConvertString.ToUInt16 "65536", None)
              (FConvertString.ToUInt16 "0xFFFFF", None)
              (FConvertString.ToUInt16 "09", None)
              (FConvertString.ToUInt16 "045", Some 37us)
              (FConvertString.ToUInt16 "0x", None) ]
            |> List.iter testEqual

        testCase "ToInt16" <| fun _ ->
            [ (FConvertString.ToInt16 "45", Some 45s)
              (FConvertString.ToInt16 "0", Some 0s)
              (FConvertString.ToInt16 "-0", Some 0s)
              (FConvertString.ToInt16 "-9", Some -9s)
              (FConvertString.ToInt16 "", None)
              (FConvertString.ToInt16 "0xA", Some 10s)
              (FConvertString.ToInt16 "0xFFFF", Some 0xFFFFs)
              (FConvertString.ToInt16 "0xG", None)
              (FConvertString.ToInt16 "0x-A", None)
              (FConvertString.ToInt16 "65536", None)
              (FConvertString.ToInt16 "0xFFFFF", None)
              (FConvertString.ToInt16 "09", None)
              (FConvertString.ToInt16 "045", Some 37s)
              (FConvertString.ToInt16 "0x", None) ]
            |> List.iter testEqual

        testCase "ToUInt32" <| fun _ ->
            [ (FConvertString.ToUInt32 "45", Some 45u)
              (FConvertString.ToUInt32 "0", Some 0u)
              (FConvertString.ToUInt32 "-0", Some 0u)
              (FConvertString.ToUInt32 "-9", None)
              (FConvertString.ToUInt32 "", None)
              (FConvertString.ToUInt32 "0xA", Some 10u)
              (FConvertString.ToUInt32 "4294967296", None)
              (FConvertString.ToUInt32 "0xFFFFFFFF", Some 4294967295u)
              (FConvertString.ToUInt32 "0xFFFFFFFFF", None)
              (FConvertString.ToUInt32 "0xG", None)
              (FConvertString.ToUInt32 "0x-A", None)
              (FConvertString.ToUInt32 "09", None)
              (FConvertString.ToUInt32 "045", Some 37u)
              (FConvertString.ToUInt32 "0x", None) ]
            |> List.iter testEqual

        testCase "ToInt32" <| fun _ ->
            [ (FConvertString.ToInt32 "45", Some 45)
              (FConvertString.ToInt32 "0", Some 0)
              (FConvertString.ToInt32 "-0", Some 0)
              (FConvertString.ToInt32 "-9", Some -9)
              (FConvertString.ToInt32 "", None)
              (FConvertString.ToInt32 "0xA", Some 10)
              (FConvertString.ToInt32 "4294967296", None)
              (FConvertString.ToInt32 "0xFFFFFFFF", Some 0xFFFFFFFF)
              (FConvertString.ToInt32 "0xFFFFFFFFF", None)
              (FConvertString.ToInt32 "0xG", None)
              (FConvertString.ToInt32 "0x-A", None)
              (FConvertString.ToInt32 "09", None)
              (FConvertString.ToInt32 "045", Some 37)
              (FConvertString.ToInt32 "0x", None) ]
            |> List.iter testEqual

        testCase "ToFloat32" <| fun _ ->
            [ (FConvertString.ToFloat32 "3.14", Some 3.14F)
              (FConvertString.ToFloat32 "0.000001", Some 0.000001F)
              (FConvertString.ToFloat32 "8.9e4", Some 8.9e4F)
              (FConvertString.ToFloat32 "8.9E4", Some 8.9E4F)
              (FConvertString.ToFloat32 "8.9E-4", Some 8.9E-4F)
              (FConvertString.ToFloat32 "-8.9E-4", Some -8.9E-4F)
              (FConvertString.ToFloat32 "0", Some 0.0F)
              (FConvertString.ToFloat32 "-0", Some 0.0F)
              (FConvertString.ToFloat32 "-3.14", Some -3.14F)
              (FConvertString.ToFloat32 "--", None) ]
            |> List.iter testFloatHighAccuracy
           
        testCase "ToSingle" <| fun _ ->
            [ (FConvertString.ToSingle "3.14", Some 3.14F)
              (FConvertString.ToSingle "0.000001", Some 0.000001F)
              (FConvertString.ToSingle "8.9e4", Some 8.9e4F)
              (FConvertString.ToSingle "8.9E4", Some 8.9E4F)
              (FConvertString.ToSingle "8.9E-4", Some 8.9E-4F)
              (FConvertString.ToSingle "-8.9E-4", Some -8.9E-4F)
              (FConvertString.ToSingle "0", Some 0.0F)
              (FConvertString.ToSingle "-0", Some 0.0F)
              (FConvertString.ToSingle "-3.14", Some -3.14F)
              (FConvertString.ToSingle "--", None) ]
            |> List.iter testFloatHighAccuracy
            
        testCase "ToFloat" <| fun _ ->
            [ (FConvertString.ToFloat "3.14", Some 3.14)
              (FConvertString.ToFloat "0.000000001", Some 0.000000001)
              (FConvertString.ToFloat "8.954321e8", Some 8.954321e8)
              (FConvertString.ToFloat "8.9E4", Some 8.9E4)
              (FConvertString.ToFloat "8.9E-4", Some 8.9E-4)
              (FConvertString.ToFloat "-8.9E-4", Some -8.9E-4)
              (FConvertString.ToFloat "0", Some 0.0)
              (FConvertString.ToFloat "-0", Some 0.0)
              (FConvertString.ToFloat "-3.14", Some -3.14)
              (FConvertString.ToFloat "--", None) ]
            |> List.iter testFloatHighAccuracy

        testCase "ToDouble" <| fun _ ->
            [ (FConvertString.ToDouble "3.14", Some 3.14)
              (FConvertString.ToDouble "0.000000001", Some 0.000000001)
              (FConvertString.ToDouble "8.954321e8", Some 8.954321e8)
              (FConvertString.ToDouble "8.9E4", Some 8.9E4)
              (FConvertString.ToDouble "8.9E-4", Some 8.9E-4)
              (FConvertString.ToDouble "-8.9E-4", Some -8.9E-4)
              (FConvertString.ToDouble "0", Some 0.0)
              (FConvertString.ToDouble "-0", Some 0.0)
              (FConvertString.ToDouble "-3.14", Some -3.14)
              (FConvertString.ToDouble "--", None) ]
            |> List.iter testFloatHighAccuracy

        testCase "ToBoolean" <| fun _ ->
            [ (FConvertString.ToBoolean "true", Some true)
              (FConvertString.ToBoolean "True", Some true)
              (FConvertString.ToBoolean "TRUE", Some true)
              (FConvertString.ToBoolean "truE", Some true)
              (FConvertString.ToBoolean "", None)
              (FConvertString.ToBoolean "Treu", None)
              (FConvertString.ToBoolean "T", None)
              (FConvertString.ToBoolean "false", Some false)
              (FConvertString.ToBoolean "False", Some false)
              (FConvertString.ToBoolean "FALSE", Some false)
              (FConvertString.ToBoolean "falsE", Some false)
              (FConvertString.ToBoolean "Fals", None)
              (FConvertString.ToBoolean "F", None)
              (FConvertString.ToBoolean "1", None)
              (FConvertString.ToBoolean "0", None) ]
            |> List.iter testEqual
    ]
    
[<EntryPoint>]
let main args =
    runTestsInAssemblyWithCLIArgs [] args
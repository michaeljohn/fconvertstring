﻿/// This is a simple module for converting a string to a primitive type
/// wrapped in an F# option type.
module FConvertString

open System


/// Convert a string to a byte.
let ToByte(str: string): byte option =
    try
        if str.Length > 1 then
            match str with
            | str when str.[0..1] = "0x" -> Convert.ToByte(str, 16) |> Some
            | str when str.[0] = '0' -> Convert.ToByte(str, 8) |> Some
            | _ -> Convert.ToByte str |> Some
        else
            Convert.ToByte str |> Some
    with
    | :? FormatException -> None
    | :? OverflowException -> None
    | _ -> None


/// Convert a string to a signed byte.
let ToSByte(str: string): sbyte option =
    try
        if str.Length > 1 then
            match str with
            | str when str.[0..1] = "0x" -> Convert.ToSByte(str, 16) |> Some
            | str when str.[0] = '0' -> Convert.ToSByte(str, 8) |> Some
            | _ -> Convert.ToSByte str |> Some
        else
            Convert.ToSByte str |> Some
    with
    | :? FormatException -> None
    | :? OverflowException -> None
    | _ -> None


/// Convert a string to a uint16.
let ToUInt16(str: string): uint16 option =
    try
        if str.Length > 1 then
            match str with
            | str when str.[0..1] = "0x" -> Convert.ToUInt16(str, 16) |> Some
            | str when str.[0] = '0' -> Convert.ToUInt16(str, 8) |> Some
            | _ -> Convert.ToUInt16 str |> Some
        else
            Convert.ToUInt16 str |> Some
    with
    | :? System.FormatException -> None
    | :? System.OverflowException -> None
    | _ -> None


/// Convert a string to a int16.
let ToInt16(str: string): int16 option =
    try
        if str.Length > 1 then
            match str with
            | str when str.[0..1] = "0x" -> Convert.ToInt16(str, 16) |> Some
            | str when str.[0] = '0' -> Convert.ToInt16(str, 8) |> Some
            | _ -> Convert.ToInt16 str |> Some
        else
            Convert.ToInt16 str |> Some
    with
    | :? System.FormatException -> None
    | :? System.OverflowException -> None
    | _ -> None


/// Convert a string to a uint32.
let ToUInt32(str: string): uint32 option =
    try
        if str.Length > 1 then
            match str with
            | str when str.[0..1] = "0x" -> Convert.ToUInt32(str, 16) |> Some
            | str when str.[0] = '0' -> Convert.ToUInt32(str, 8) |> Some
            | _ -> Convert.ToUInt32 str |> Some
        else
            Convert.ToUInt32 str |> Some
    with
    | :? System.FormatException -> None
    | :? System.OverflowException -> None
    | _ -> None


/// Convert a string to a int32.
let ToInt32(str: string): int32 option =
    try
        if str.Length > 1 then
            match str with
            | str when str.[0..1] = "0x" -> Convert.ToInt32(str, 16) |> Some
            | str when str.[0] = '0' -> Convert.ToInt32(str, 8) |> Some
            | _ -> Convert.ToInt32 str |> Some
        else
            Convert.ToInt32 str |> Some
    with
    | :? System.FormatException -> None
    | :? System.OverflowException -> None
    | _ -> None


/// Convert a string to a 32-bit float (single).
let ToFloat32(str: string): float32 option =
    let ok, value = Single.TryParse str
    match ok with
    | true -> Some value
    | false -> None

let ToSingle = ToFloat32


/// Convert a string to a 64-bit float (double).
let ToFloat(str: string): float option =
    let ok, value = Double.TryParse str
    match ok with
    | true -> Some value
    | false -> None

let ToDouble = ToFloat


/// Convert a string to a boolean
let ToBoolean(str: string): Boolean option =
    let ok, value = Boolean.TryParse str
    match ok with
    | true -> Some value
    | false -> None
